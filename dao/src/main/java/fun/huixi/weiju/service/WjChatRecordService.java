package fun.huixi.weiju.service;

import fun.huixi.weiju.pojo.entity.WjChatRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 聊天室-聊天记录 服务类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
public interface WjChatRecordService extends IService<WjChatRecord> {

}
