package fun.huixi.weiju.service.impl;

import cn.hutool.core.util.ObjectUtil;
import fun.huixi.weiju.exception.BusinessException;
import fun.huixi.weiju.pojo.entity.WjDynamicComment;
import fun.huixi.weiju.mapper.WjDynamicCommentMapper;
import fun.huixi.weiju.service.WjDynamicCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 动态评论表 服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Service
public class WjDynamicCommentServiceImpl extends ServiceImpl<WjDynamicCommentMapper, WjDynamicComment> implements WjDynamicCommentService {

    /**
     * 判断这条评论是否是这位用户的
     *
     * @param userId           用户id
     * @param dynamicCommentId 动态评论id
     * @return java.lang.Boolean
     * @Author 叶秋
     * @Date 2021/11/29 14:52
     **/
    @Override
    public Boolean judgeCommentOrMe(Integer userId, Integer dynamicCommentId) {

        WjDynamicComment byId = this.getById(dynamicCommentId);

        if(ObjectUtil.isNull(byId)){
            throw new BusinessException("该条评论不存在");
        }

       if(byId.getUserId().equals(userId)){
           return true;
       }

        return false;
    }
}
