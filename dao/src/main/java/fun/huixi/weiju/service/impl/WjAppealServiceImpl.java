package fun.huixi.weiju.service.impl;

import fun.huixi.weiju.pojo.entity.WjAppeal;
import fun.huixi.weiju.mapper.WjAppealMapper;
import fun.huixi.weiju.service.WjAppealService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 诉求表 服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Service
public class WjAppealServiceImpl extends ServiceImpl<WjAppealMapper, WjAppeal> implements WjAppealService {

}
