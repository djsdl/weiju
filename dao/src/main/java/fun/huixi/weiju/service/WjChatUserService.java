package fun.huixi.weiju.service;

import fun.huixi.weiju.pojo.entity.WjChatUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 聊天室对应的用户 服务类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
public interface WjChatUserService extends IService<WjChatUser> {

}
