package fun.huixi.weiju.service;

import fun.huixi.weiju.pojo.entity.WjUserTagCenter;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户与标签的中间表 服务类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
public interface WjUserTagCenterService extends IService<WjUserTagCenter> {

}
