package fun.huixi.weiju.mapper;

import fun.huixi.weiju.pojo.entity.WjChatRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 聊天室-聊天记录 Mapper 接口
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
public interface WjChatRecordMapper extends BaseMapper<WjChatRecord> {

}
