package fun.huixi.weiju.mapper;

import fun.huixi.weiju.pojo.entity.WjUserTag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户所存储的标签 Mapper 接口
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
public interface WjUserTagMapper extends BaseMapper<WjUserTag> {

}
