package fun.huixi.weiju.controller;


import fun.huixi.weiju.business.AppealBusiness;
import fun.huixi.weiju.page.PageData;
import fun.huixi.weiju.pojo.dto.appeal.PageQueryAppealCommentDTO;
import fun.huixi.weiju.pojo.dto.appeal.SaveAppealCommentDTO;
import fun.huixi.weiju.pojo.vo.appeal.AppealCommentItemVO;
import fun.huixi.weiju.util.CommonUtil;
import fun.huixi.weiju.util.wrapper.ResultData;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import fun.huixi.weiju.base.BaseController;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 诉求-评论 前端控制器
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@RestController
@RequestMapping("/wjAppealComment")
public class WjAppealCommentController extends BaseController {

    @Resource
    private AppealBusiness appealBusiness;


    /**
     *  保存帖子的评论
     * @Author 叶秋 
     * @Date 2021/11/16 11:33
     * @return fun.huixi.weiju.util.wrapper.ResultData
     **/
    @PostMapping("saveAppealComment")
    public ResultData saveAppealComment(@RequestBody @Valid SaveAppealCommentDTO saveAppealCommentDTO){

        Integer userId = CommonUtil.getNowUserId();

        appealBusiness.saveAppealComment(userId, saveAppealCommentDTO);

        return ResultData.ok();
    }



    /**
     *  分页查询帖子评论
     * @Author 叶秋
     * @Date 2021/11/16 11:53
     * @param pageQueryAppealCommentDTO 分页查询参数
     * @return fun.huixi.weiju.util.wrapper.ResultData
     **/
    @PostMapping("pageQueryAppealComment")
    public ResultData<PageData<AppealCommentItemVO>> pageQueryAppealComment(@RequestBody @Valid PageQueryAppealCommentDTO pageQueryAppealCommentDTO){

        Integer userId = CommonUtil.getNowUserId();

        PageData<AppealCommentItemVO> pageData = appealBusiness.pageQueryAppealComment(userId, pageQueryAppealCommentDTO);

        return ResultData.ok(pageData);
    }




}

