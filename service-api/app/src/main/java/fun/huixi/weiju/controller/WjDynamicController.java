package fun.huixi.weiju.controller;

import fun.huixi.weiju.business.DynamicBusiness;
import fun.huixi.weiju.pojo.dto.dynamic.QueryDynamicDTO;
import fun.huixi.weiju.pojo.dto.dynamic.SaveDynamicDTO;
import fun.huixi.weiju.util.CommonUtil;
import fun.huixi.weiju.util.wrapper.ResultData;
import org.springframework.web.bind.annotation.*;

import fun.huixi.weiju.base.BaseController;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 动态表 前端控制器
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@RestController
@RequestMapping("/wjDynamic")
public class WjDynamicController extends BaseController {

    @Resource
    private DynamicBusiness dynamicBusiness;




    /**
     *  保存动态
     * @Author 叶秋
     * @Date 2021/11/29 15:08
     * @param
     * @return fun.huixi.weiju.util.wrapper.ResultData
     **/
    @PostMapping("saveDynamic")
    public ResultData saveDynamic(@RequestBody SaveDynamicDTO saveDynamicDTO){

        Integer userId = CommonUtil.getNowUserId();

        dynamicBusiness.saveDynamic(userId, saveDynamicDTO);

        return ResultData.ok();
    }




    /**
     * 查询动态列表
     *
     * @return fun.huixi.weiju.util.wrapper.ResultData
     * @Author Zol
     * @Date 2021/11/10
     **/
    @PostMapping("getDynamicList")
    public ResultData getDynamicList(@RequestBody @Valid QueryDynamicDTO param) {
        Integer userId = CommonUtil.getNowUserId();
        return ResultData.ok(dynamicBusiness.listPageDynamic(userId, param));
    }





}

