## 汇溪和他们的小伙伴们

> **汇溪**： 汇溪成海之意，象征汇聚多方力量。



时间 **2021年3月29日15:39:21**   小程序端方面准备用**uni-app** 重构，短时间内后台变动会比较大，不建议拉下来学习。



## 项目配置


1. 数据库 （mysql）

   ```
   在项目内 微距项目数据库备份
   
   ```


2.  **SpringSecurity**
    ```
    POST
    登录接口为：localhost:8001/weiju-app/login/userLogin?nickName=yeqiu&password=123456
      
    ```


## 项目结构简介

```
weiju
├── common -- 通用的公共模块
├── dao -- 数据库相关方法
├── mybatis-generator -- 代码生成
├── service-api -- 对外提供的接口
├	└── app app端接口


```



## 项目注意事项（有什么好规范可写在下方）

1. 记得在IDE上下载阿里巴巴的编码检测	Alibaba Java Coding Guidelines，**力求规范**

2. 自己写过的代码，必须有Javadoc 注释（嫌写着麻烦的同学，推荐IDEA一款插件easy-javadoc，可以一键生成注释）。可参照下面模板；最重要的是 **作用，作者，时间**

   ```java
   	/**
        * 获取用户加密信息 并且保存
        * @Author 叶秋
        * @Date 2020/2/5 13:44
        **/
   ```

