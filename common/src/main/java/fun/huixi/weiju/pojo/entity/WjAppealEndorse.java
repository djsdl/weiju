package fun.huixi.weiju.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import fun.huixi.weiju.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 此表为记录诉求点赞用途，用来判断此人是否已经点赞
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wj_appeal_endorse")
public class WjAppealEndorse extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 此表的id
     */
    @TableId(value = "appeal_endorse_id", type = IdType.AUTO)
    private Integer appealEndorseId;

    /**
     * 对应的诉求id
     */
    @TableField("appeal_id")
    private Integer appealId;

    /**
     * 点赞人的id
     */
    @TableField("user_id")
    private Integer userId;



}
