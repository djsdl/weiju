package fun.huixi.weiju.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import fun.huixi.weiju.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 动态的点赞表
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wj_dynamic_endorse")
public class WjDynamicEndorse extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 动态点赞表的id
     */
    @TableId(value = "dynamic_endorse_id", type = IdType.AUTO)
    private Integer dynamicEndorseId;

    /**
     * 动态id
     */
    @TableField("dynamic_id")
    private Integer dynamicId;

    /**
     * 点赞人的id
     */
    @TableField("user_id")
    private Integer userId;



}
