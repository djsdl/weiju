package fun.huixi.weiju.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import fun.huixi.weiju.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 聊天室-聊天记录
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wj_chat_record")
public class WjChatRecord extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 主键
     */
    @TableId(value = "chat_record_id", type = IdType.AUTO)
    private Integer chatRecordId;

    /**
     * 聊天室id
     */
    @TableField("chat_id")
    private Integer chatId;

    /**
     * 用户id
     */
    @TableField("user_id")
    private Integer userId;

    /**
     * 聊天的次序（谁说在前，谁说在后）
     */
    @TableField("chat_order")
    private Integer chatOrder;

    /**
     * 聊天的内容
     */
    @TableField("content")
    private String content;

    /**
     * 内容类型（1：文字 2：图片 3：视频）
     */
    @TableField("type")
    private Integer type;



}
