package fun.huixi.weiju.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import fun.huixi.weiju.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 诉求-对应标签
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wj_appeal_tag")
public class WjAppealTag extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 主键
     */
    @TableId(value = "tag_id", type = IdType.AUTO)
    private Integer tagId;

    /**
     * 标签的名字
     */
    @TableField("tag_name")
    private String tagName;

    /**
     * 标签的解释（冗余）
     */
    @TableField("explains")
    private String explains;

    /**
     * 标签可能需要的地址值(冗余)
     */
    @TableField("url")
    private String url;



}
