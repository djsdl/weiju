package fun.huixi.weiju.util;

import cn.hutool.core.text.StrBuilder;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

/**
 *  通用的帮助类
 * @Author 叶秋
 * @Date 2020/6/15 23:12
 * @param
 * @return
 **/
public class CommonUtil {


    /**
     *  获取现在正在 调用这个接口的用户id
     * @Author 叶秋
     * @Date 2020/6/18 23:21
     * @return java.lang.String
     **/
    public static Integer getNowUserId(){

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String s = authentication.getCredentials().toString();

        if(StrUtil.isEmpty(s)){
            return null;
        }

        Integer userId = Integer.valueOf(s);

        return userId;

    }


    /**
     *  获取一个带有时间格式ide订单号码
     * @Author 叶秋
     * @Date 2021/4/22 16:26
     * @return java.lang.String
     **/
    public static String getOrderNo(){
        //格式化当前时间
        SimpleDateFormat sfDate = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String strDate = sfDate.format(new Date());
        //得到17位时间如：20170411094039080
        System.out.println("时间17位：" + strDate);
        //为了防止高并发重复,再获取3个随机数
        String random = getRandom620(3);

        //最后得到20位订单编号。
        return strDate + random;
    }

    /**
     * 获取6-10 的随机位数数字
     * @param length	想要生成的长度
     * @return result
     */
    public static String getRandom620(Integer length) {
        String result = "";
        Random rand = new Random();
        int n = 20;
        if (null != length && length > 0) {
            n = length;
        }
        int randInt = 0;
        for (int i = 0; i < n; i++) {
            randInt = rand.nextInt(10);
            result += randInt;
        }
        return result;
    }


    /**
     *  去掉字符串中的emoji
     * @Author 叶秋
     * @Date 2021/8/24 11:54
     * @return java.lang.String
     **/
    public static String filterEmoji(String source, String slipStr) {
        if(StringUtils.isNotBlank(source)){
            return source.replaceAll("[\\ud800\\udc00-\\udbff\\udfff\\ud800-\\udfff]", slipStr);
        }else{
            return source;
        }
    }

    /**
     *  获取4位数的验证码
     * @Author 叶秋
     * @Date 2021/6/26 17:46
     * @return java.lang.String
     **/
    public static String getSMSVerification(){

        Integer[] randomNumber = {1,2,3,4,5,6,7,8,9,0};


        StrBuilder strBuilder = new StrBuilder();

        for (int i = 0; i < 4; i++) {
            Integer code = RandomUtil.randomEle(randomNumber);
            strBuilder.append(code);
        }

        return strBuilder.toString();

    }


    /**
     *  随机获取26个字母组合
     * @Author 叶秋
     * @Date 2021/6/18 11:56
     * @param number 这个随机字母多少位
     * @return java.lang.String
     **/
    public static String getRandomLetter(Integer number) {

        String string = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        StringBuffer buff = new StringBuffer();

        for (int i = 1; i <= number; i++) {

            char str = string.charAt((int) (Math.random() * 26));

            buff.append(str);

        }

        return buff.toString();

    }

    /**
     *  判断这个文件名是否是图片
     * @Author 叶秋
     * @Date 2021/9/17 12:59
     * @param originalFilename 文件类型
     * @return java.lang.Boolean
     **/
    public static Boolean judgePicture(String originalFilename) {

        String substring = originalFilename.substring(originalFilename.lastIndexOf(".")+1);

        ArrayList<String> objects = new ArrayList<>();
        objects.add("png");
        objects.add("jpg");
        objects.add("jpeg");

        String toLowerCase = substring.toLowerCase();

        boolean contains = objects.contains(toLowerCase);

        return contains;

    }




    /**
     *  判断文件是否是视频
     * @Author 叶秋
     * @Date 2021/9/26 20:44
     * @param originalFilename 文件类型
     * @return java.lang.Boolean
     **/
    public static Boolean judgeVideo(String originalFilename){

        String substring = originalFilename.substring(originalFilename.lastIndexOf(".")+1);

        ArrayList<String> objects = new ArrayList<>();
        objects.add("mp4");

        String toLowerCase = substring.toLowerCase();

        boolean contains = objects.contains(toLowerCase);

        return contains;


    }



    /**
     *  判断这个帖子的素材 是否是符合类型的
     * @Author 叶秋
     * @Date 2021/9/25 17:58
     * @param originalFilename
     * @return java.lang.Boolean
     **/
    public static Boolean judgePostMaterial(String originalFilename){

        String substring = originalFilename.substring(originalFilename.lastIndexOf(".")+1);

        ArrayList<String> objects = new ArrayList<>();
        objects.add("png");
        objects.add("jpg");
        objects.add("jpeg");
        objects.add("mp4");

        String toLowerCase = substring.toLowerCase();

        boolean contains = objects.contains(toLowerCase);

        return contains;



    }





}
