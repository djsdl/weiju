package fun.huixi.weiju.exception;

import fun.huixi.weiju.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import fun.huixi.weiju.util.wrapper.ResultData;

/**
 * <p>
 * 全局异常处理器
 * </p>
 *
 * @author xzl
 * @since 2020-01-16
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 参数非法异常.
     *
     * @param e the e
     * @return the wrapper
     */
    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ResultData<String> constraintViolationException(MethodArgumentNotValidException e) {
        log.error("参数非法异常={}", e.getMessage());
        System.out.println(e.getBindingResult().getFieldError().getDefaultMessage());
        return ResultData.error(e.getBindingResult().getFieldError().getDefaultMessage());
    }

    /**
     * 业务异常
     *
     * @return
     */
    @ExceptionHandler(BusinessException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ResultData<String> myException(BusinessException e) {
        log.error("业务异常={}", e.getMessage(), e);
        System.out.println(e.getMessage());
        return ResultData.error(e.getMessage());
    }

    /**
     * 全局异常
     *
     * @return
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ResultData<Object> exception(Exception e) {

        log.error("全局异常={}", e.getMessage(), e);
        //可做入库处理
        return ResultData.error();
    }

}
