package fun.huixi.weiju.constant;

/**
 * 常量
 * @author 叶秋
 * @Date 2021年4月10日14:44:57
 */
public interface Constant {

    /**
     *  删除标识 N 没有删除
     * @Author 叶秋
     * @Date 2020/5/5 11:04
     **/
    Integer FALSE = 0;
    String FALSE_STR = "0";

    /**
     *  删除标识 Y 标识已经删除
     * @Author 叶秋
     * @Date 2020/5/5 11:04
     **/
    Integer TRUE = 1;
    String TRUE_STR = "1";

    /**
     *  删除标识符的默认值
     **/
    Boolean B_FALSE = false;


    /**
     * 状态码
     **/
    Integer STATUS_FLAG = 0;


    /**
     * 乐观锁 测试版本值
     **/
    Integer VERSION = 1;




}